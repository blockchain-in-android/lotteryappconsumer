package net.anandu.lotteryappconsumer

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import dev.burnoo.compose.rememberpreference.rememberStringPreference
import net.anandu.blocklib.BlockLib
import net.anandu.blocklib.blockchain.Encryption
import net.anandu.blocklib.database.Transaction
import net.anandu.blocklib.util.PoAGenesisConfig
import net.anandu.lotteryappconsumer.ui.screens.check_winner.CheckWinnerPage
import net.anandu.lotteryappconsumer.ui.screens.claim_lottery.ClaimPage
import net.anandu.lotteryappconsumer.ui.theme.LotteryAppConsumerTheme
import java.math.BigInteger
import java.security.MessageDigest
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


suspend fun BlockLib.queryTransactions(query: String): List<Transaction> =
    suspendCoroutine { cont ->
        val callback = { l: List<Transaction> -> cont.resume(l) }
        this.searchInTransactionData(query, callback)
    }

fun getRandomString(length: Int): String {
    val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
    return (1..length)
        .map { allowedChars.random() }
        .joinToString("")
}

fun md5(input: String): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
}

suspend fun BlockLib.claimTicket(
    lotteryId: String,
    publicName: String,
    ticketId: String,
    secret: String
) {
    // todo validate the transaction

    val ticketTransactions =
        this.queryTransactions(ticketId).filter { it.data["type"] == "CLAIM_TICKET" }

    if (ticketTransactions.isNotEmpty()) {
        throw Exception("Ticket has already been claimed")
    }

    this.createTransaction(
        "SELF", "ALL", 0, mapOf(
            "type" to "CLAIM_TICKET",
            "lotteryId" to lotteryId,
            "publicName" to publicName,
            "ticketId" to ticketId,
            "secret" to md5(ticketId + secret)
        )
    );
}

suspend fun BlockLib.checkWinner(lotteryId: String, ticketId: String, secret: String): Boolean {
    // find the transaction of the lottery
    val winner = this.queryTransactions(lotteryId).firstOrNull { it.data["type"] == "DRAW_LOTTERY" }
        ?: throw Exception("Winner has not been declared yet")

    val computedSecret = md5(ticketId + secret)
    println(winner)
    println("lotteryId= $lotteryId, ticketId= $ticketId secret=$secret computed: $computedSecret")
    if (winner.data["winnerHash"] == computedSecret) {
        return true;
    }
    return false;
}


class MainActivity : ComponentActivity() {
    @ExperimentalComposeUiApi
    @ExperimentalPermissionsApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LotteryAppConsumerTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    MainPage()
                }
            }
        }
    }
}

@ExperimentalPermissionsApi
@ExperimentalComposeUiApi
@Composable
fun MainPage() {
    val context = LocalContext.current
    val navController = rememberNavController()
    val encryption = (object : Encryption() {});
    var publicKey by rememberStringPreference(
        keyName = "publicKey", // preference is stored using this key
        initialValue = null, // returned before preference is loaded
        defaultValue = "", // returned when preference is not set yet
    )
    var privateKey by rememberStringPreference(
        keyName = "privatekey", // preference is stored using this key
        initialValue = null, // returned before preference is loaded
        defaultValue = "", // returned when preference is not set yet
    )
    if (privateKey == "" && publicKey == "") {
        println("Generating new keyPair")
        privateKey = encryption.privateKeyString;
        publicKey = encryption.pubKeyString;
    }
    if (privateKey == null || publicKey == null || publicKey == "" || privateKey == "") {
        Text("Loading")
        return;
    }
    println("privateKey: $privateKey")
    println("publicKey: $publicKey")

    val kp = Encryption.getKeyPairFromEncoded(publicKey, privateKey)

    val blockLib = object : BlockLib() {
        override fun onConnected() {
            println("Not yet implemented")
        }

        override fun onTransactionAdded(t: Transaction?) {
            println("Not yet implemented")
        }

        override fun onSyncComplete() {
            println("Not yet implemented")
        }

        override fun onBlockAdded(blockNum: Int?) {
            Toast.makeText(context, "New Block Added $blockNum", Toast.LENGTH_LONG).show()
            println("Not yet implemented")
        }

    }
    val peerId = getRandomString(10);
    val genConfig = PoAGenesisConfig(listOf(), "", 5);
    blockLib.init(LocalContext.current, peerId, kp, genConfig, fun(t: Transaction): Boolean {
        if (t.timestamp == "0") {
            return false;
        }
        return true;
    })

    Column(Modifier.fillMaxSize(), verticalArrangement = Arrangement.Top) {
        TopAppBar(
            elevation = 4.dp,
            title = {
                Text("LotteryApp")
            },
            backgroundColor = MaterialTheme.colors.primarySurface,
            actions = {
                IconButton(onClick = {
                    navController.navigate("claimPage")
                }) {
                    Icon(Icons.Filled.AccountCircle, null)
                }
                IconButton(onClick = {
                    navController.navigate("checkWinner")
                }) {
                    Icon(Icons.Filled.ConfirmationNumber, null)
                }
            })
        NavHost(navController = navController, startDestination = "claimPage") {
//        composable("scanPage") { ScanQRPage(navController)}
            composable("claimPage") { ClaimPage(navController, blockLib) }
            composable("checkWinner") { CheckWinnerPage(navController, blockLib) }
        }

    }
}

@ExperimentalComposeUiApi
@ExperimentalPermissionsApi
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    LotteryAppConsumerTheme {
        MainPage()
    }
}