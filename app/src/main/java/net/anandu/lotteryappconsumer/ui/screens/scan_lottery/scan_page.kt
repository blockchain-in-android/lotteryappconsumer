package net.anandu.lotteryappconsumer.ui.screens.scan_lottery

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.util.Log
import android.widget.Toast
import androidx.camera.core.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.zxing.*
import com.google.zxing.common.HybridBinarizer
import com.google.zxing.qrcode.QRCodeReader
import net.anandu.lotteryappconsumer.ui.common.CameraCapture
import net.anandu.lotteryappconsumer.ui.common.Permission


@ExperimentalComposeUiApi
@ExperimentalPermissionsApi
@Composable
fun ScanQRPage(navController: NavHostController) {
    // todo: implement AndroidView thing for camera
    val context = LocalContext.current

    Permission(
        permission = android.Manifest.permission.CAMERA,
        rationale = "You said you wanted a picture, so I'm going to have to ask for permission.",
        permissionNotAvailableContent = {
            Column() {
                Text("O noes! No Camera!")
                Spacer(modifier = Modifier.height(8.dp))
                Button(onClick = {
                    context.startActivity(Intent(ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                        data = Uri.fromParts("package", context.packageName, null)
                    })
                }) {
                    Text("Open Settings")
                }
            }
        }
    ) {
        Text("It worked!")
        CameraCapture(onImageFile = {
            file ->
            run {
                val myBitmap: Bitmap = BitmapFactory.decodeFile(file.absolutePath)
                println(myBitmap)
                val decoded = scanQRImage(myBitmap);
                Log.i("QrTest", "Decoded string=$decoded");
                Toast.makeText(
                    context,
                    "decoded = $decoded",
                    Toast.LENGTH_SHORT
                ).show()

                if ( decoded != null) {
                    navController.navigate("claimPage")
                }
            }
        })
    }
}

fun scanQRImage(bMap: Bitmap): String? {
    var contents: String? = null
    val intArray = IntArray(bMap.width * bMap.height)
    //copy pixel data from the Bitmap into the 'intArray' array
    bMap.getPixels(intArray, 0, bMap.width, 0, 0, bMap.width, bMap.height)
    val source: LuminanceSource = RGBLuminanceSource(bMap.width, bMap.height, intArray)
    val bitmap = BinaryBitmap(HybridBinarizer(source))
    val reader: Reader = QRCodeReader()
    try {
        val result: Result = reader.decode(bitmap)
        println(result)
        contents = result.text
    } catch (e: Exception) {
        Log.e("QrTest", "Error decoding barcode", e)
    }
    return contents
}

fun processDecodedText(text: String): String {
    // if valid string
    // check the time
    // check chain if claimed
    // if ( expired and claimed)
    //  show checkwinner
    // if ( !expired )
    //  show claimTicket
    // else throw
    return ""
}
