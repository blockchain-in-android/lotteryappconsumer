package net.anandu.lotteryappconsumer.ui.common

import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp

@Composable
fun ItemStyleText(field: String, value: String) {
    Text(
        buildAnnotatedString {
            withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                append(field.uppercase())
            }
            append(" : ")
            withStyle(style = SpanStyle(color = Color.Gray)) {
                append(value)
            }
        }
    )
    Spacer(Modifier.size(4.dp))
}

@Composable
fun LotteryCard(name: String, date: String, ticketId: String) {
    Column(
        Modifier
            .fillMaxWidth()
            .padding(horizontal = 0.dp, vertical = 16.dp)
            .height(180.dp)
    ) {
        Card(elevation = 8.dp, modifier= Modifier.fillMaxHeight()) {
            Column(
                Modifier
                    .fillMaxWidth()
                    .fillMaxHeight()
                    .padding(16.dp)
            ) {
                Spacer(Modifier.size(60.dp))
                ItemStyleText(field = "Lottery name", value = name)
//                ItemStyleText(field = "Lottery date", value = date)
                ItemStyleText(field = "Ticket Id", value = ticketId)
            }
        }
    }
}
