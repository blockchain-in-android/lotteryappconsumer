package net.anandu.lotteryappconsumer.ui.screens.check_winner

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.OutlinedTextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import kotlinx.coroutines.launch
import net.anandu.blocklib.BlockLib
import net.anandu.lotteryappconsumer.checkWinner
import net.anandu.lotteryappconsumer.ui.common.LotteryCard

@Composable
fun CheckWinnerPage(navController: NavHostController, blockLib: BlockLib) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.Center
    ) {
        val context = LocalContext.current
        val coroutineScope = rememberCoroutineScope()
        var lotteryId by remember { mutableStateOf("") }
        var ticketId by remember { mutableStateOf("") }
        var secret by remember { mutableStateOf("") }
        Text(
            text = "Check Winner",
            style = MaterialTheme.typography.h4
        )
        Spacer(Modifier.size(16.dp))

        LotteryCard(name = lotteryId, date = "12/12/2021", ticketId = ticketId)

        OutlinedTextField(
            value = lotteryId,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            onValueChange = { newText ->
                lotteryId = newText
            },
            label = {
                Text(text = "LotteryId")
            }
        )

        OutlinedTextField(
            value = ticketId,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            onValueChange = { newText ->
                ticketId = newText
            },
            label = {
                Text(text = "TicketId")
            }
        )
        OutlinedTextField(
            value = secret,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            onValueChange = { newText ->
                secret = newText
            },
            label = {
                Text(text = "Secret")
            }
        )
        Spacer(Modifier.size(20.dp))
        Button(onClick = {
            coroutineScope.launch {
                try {
                    val isWinner = blockLib.checkWinner(
                        lotteryId,
                        ticketId,
                        secret,
                    )
                    if ( isWinner ) {
                        Toast.makeText(context, "YES YOU ARE WINNER", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(context, ":( :(", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
                }
            }
        }) {
            Text(text = "Check")
        }
    }
}
