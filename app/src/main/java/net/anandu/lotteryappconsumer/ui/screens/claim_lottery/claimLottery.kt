package net.anandu.lotteryappconsumer.ui.screens.claim_lottery

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.OutlinedTextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import kotlinx.coroutines.launch
import net.anandu.blocklib.BlockLib
import net.anandu.lotteryappconsumer.claimTicket
import net.anandu.lotteryappconsumer.ui.common.LotteryCard
import java.lang.Exception

@Composable
fun ClaimPage(navController: NavHostController, blockLib: BlockLib) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.Center
    ) {
        val context = LocalContext.current
        val coroutineScope = rememberCoroutineScope()
        var lotteryId by remember { mutableStateOf("") }
        var ticketId by remember { mutableStateOf("") }
        var secret by remember { mutableStateOf("") }
        var name by remember { mutableStateOf("") }

        Text(
            text = "Claim Ticket",
            style = MaterialTheme.typography.h4
        )
        Spacer(Modifier.size(16.dp))

        LotteryCard(name = lotteryId, date = "12/12/2021", ticketId = ticketId )

        OutlinedTextField(
            value = lotteryId,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            onValueChange = { newText ->
                lotteryId = newText
            },
            label = {
                Text(text = "Lottery Id")
            }
        )
        OutlinedTextField(
            value = ticketId,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            onValueChange = { newText ->
                ticketId = newText
            },
            label = {
                Text(text = "Ticket ID")
            }
        )

        OutlinedTextField(
            value = secret,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            onValueChange = { newText ->
                secret = newText
            },
            label = {
                Text(text = "Secret")
            }
        )
        OutlinedTextField(
            value = name,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            onValueChange = { newText ->
                name = newText
            },
            label = {
                Text(text = "Name")
            }
        )

        Spacer(Modifier.size(20.dp))
        Button(onClick = {
            coroutineScope.launch {
                try {
                    blockLib.claimTicket(
                        lotteryId,
                        name,
                        ticketId,
                        secret
                    )
                } catch( e: Exception) {
                    Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
                }
                Toast.makeText(context, "Ticket claimed successfully", Toast.LENGTH_LONG).show()
            }
        }) {
            Text(text = "Claim")
        }
    }
}

